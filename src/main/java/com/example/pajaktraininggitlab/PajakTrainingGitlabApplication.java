package com.example.pajaktraininggitlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@RestController
public class PajakTrainingGitlabApplication {

	@GetMapping("/")
	public String getHelloWorld(){
		String otherResponse = " overdress";
		return "Hello World Hey Hey" + otherResponse;
	}

	@GetMapping("/")
	public String getSabatsiSebat(){
		String otherResponse = " overdress";
		return "Sabat suka Sebat dulu bosssss" + otherResponse;
	}

	@GetMapping("/v3")
	public String getHelloWorldV3(){
		return "my other new hello world";
	}

	@GetMapping("/v4")
	public String getHelloWorldV4(){
		return "my other new hello world v4";
	}

	@GetMapping("/hello-world/wahyu")
	public String getHelloByName(@PathVariable String name) {
		return "Hello Wahyu Rio";
	}

	@GetMapping("/v5")
	public String getHelloByName() {
		return "Hello azhar gelo yeah";
	}

	public static void main(String[] args) {
		SpringApplication.run(PajakTrainingGitlabApplication.class, args);
	}

}
